import React from 'react';
import './HomePage.css';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import Project from "../components/Project";

const HomePage = () => {
    return (
        <div className='layoutContainer mt-widescreen-10'>
            <div style={{flex: 1}}>
                <div className='main-title'>
                    <h1 className='title'>Самые интересные проекты</h1>
                    <div className='recommend'>
                        <span>Сортировать</span>
                        <ArrowDropDownIcon/>
                    </div>
                </div>
                <Project/>
                <Project/>
                <Project/>
                <Project/>
                <Project/>
                <Project/>
            </div>
            <div className='sidebarWithSeparator'>
                <div>Последние истории</div>
                <div>Обсуждение</div>
                <div>Новые проекты</div>
                <div>Вакансии</div>
            </div>

        </div>
    );
};

export default HomePage;