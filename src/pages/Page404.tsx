import React from 'react';

const Page404 = () => {
    return (
        <div className='pageWrapper'>
            <h1>Page Not Found</h1>
        </div>
    );
};

export default Page404;