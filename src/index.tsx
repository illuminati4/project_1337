import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import React from 'react';
import ReactDOM from 'react-dom/client';
// import {Provider} from "react-redux";
// import {store} from "./store";
import {BrowserRouter as Router} from "react-router-dom";
import './index.css';
import App from './App';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <React.StrictMode>
      {/*<Provider store={store}>*/}
          <Router>
              <App/>
          </Router>
      {/*</Provider>*/}
  </React.StrictMode>
);

