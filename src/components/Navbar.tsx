import React from 'react'
import {Link} from "react-router-dom";
import './Navbar.css';
import TipsAndUpdatesIcon from '@mui/icons-material/TipsAndUpdates';
import InputBase from '@mui/material/InputBase';
import SearchIcon from '@mui/icons-material/Search';
import Button from '@mui/material/Button';


const Navbar = () => {
    return (
        <div className='navbar px-widescreen-8 px-desktop-5'>
            <Link className='navLink logo' to='/'><TipsAndUpdatesIcon sx={{fontSize: '32px'}}/></Link>
            <div className='searchInput'>
                <SearchIcon sx={{color: 'grey'}}/>  
                <InputBase
                    sx={{ml: 1, flex: 1}}
                    placeholder="Поиск проектов"
                    inputProps={{'aria-label': 'search projects'}}
                />
            </div>
            <div className='navLinksBlock'>
                <div className='navBarLink'>
                    <Link className='navLink' to='/'>Проекты</Link>
                </div>
                <div className='navBarLink'>
                    <Link className='navLink' to='/'>Сообщество</Link>
                </div>
                <div className='navBarLink'>
                    <Link className='navLink' to='/'>Инструменты</Link>
                </div>
                <div className='navBarLink'>
                    <Link className='navLink' to='/'>Вакансии</Link>
                </div>
                <div className='navBarLink'>
                    <Link className='navLink' to='/'>О нас</Link>
                </div>
            </div>
            <div className='navLinksBlock right'>
                <Link className='navBarLink question' to='/'>Как опубликовать проект?</Link>
                <Link className='navBarLink' to='/login'>Войти</Link>
                <button className='navBarButton'>
                    <Link className='navBarLink' to='/signup'>Зарегистрироваться</Link>
                </button>
            </div>
        </div>
    )
}

export default Navbar