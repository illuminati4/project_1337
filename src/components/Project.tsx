import React from 'react';
import logo1 from "../images/4795fa98-c929-4081-8fa5-f989edb72243.avif";
import ModeCommentIcon from "@mui/icons-material/ModeComment";
import ArrowDropUpIcon from "@mui/icons-material/ArrowDropUp";

const Project = () => {
    return (
        <div style={{paddingTop: '24px', paddingBottom: '24px'}}>
            <div className='project'>
                <div className='project-left'>
                    <div className='project-logo'>
                        <img className='logo' src={logo1} alt="avatar"/>
                    </div>
                    <div className='project-description'>
                        <div className='project-title'>Lorem ipsum</div>
                        <div className='project-about'>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                            sed do
                        </div>
                        <div className='project-details'>
                            <div className='project-details-comments'>
                                <ModeCommentIcon/>
                                <div>95</div>
                            </div>
                            <div className='project-details-comments'>
                                Web
                            </div>
                        </div>
                    </div>
                </div>
                <div className='project-rating'>
                    <button className='vote-button'>
                        <div>
                            <ArrowDropUpIcon/>
                        </div>
                        <div>210</div>
                    </button>
                </div>

            </div>
        </div>
    );
};

export default Project;